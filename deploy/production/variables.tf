# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

variable "cloudflare_api_token" {
  type = string

  sensitive = true
}

variable "google_cloud_service_account_file" {
  type    = string
  default = "credentials.json"
}
