# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

locals {
  deployment   = "dyff-cert-manager"
  cluster_name = "${var.environment}-dyff-cloud"
  name         = "${var.environment}-${local.deployment}"
  region       = "us-central1"

  default_tags = {
    deployment  = local.deployment
    environment = var.environment
  }

  crd_path  = "${path.module}/crds/"
  crd_files = fileset("${local.crd_path}", "*.yaml")

  cloudflare_secret_key_name = "cloudflare-api-token" # pragma: allowlist secret

  issuer_email = "service@dsri.org"
}
