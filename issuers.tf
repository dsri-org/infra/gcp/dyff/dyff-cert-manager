# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

# https://cert-manager.io/docs/configuration/acme/dns01/cloudflare/
resource "kubernetes_secret" "cloudflare_api_token" {
  metadata {
    name      = "cloudflare-api-token"
    namespace = kubernetes_namespace.cert_manager.metadata.0.name
  }
  data = {
    "${local.cloudflare_secret_key_name}" = var.cloudflare_api_token
  }
  type = "Opaque"
}

resource "kubernetes_manifest" "production_issuer" {
  manifest = {
    apiVersion = "cert-manager.io/v1"
    kind       = "ClusterIssuer"
    metadata = {
      name = "letsencrypt-production"
    }
    spec = {
      acme = {
        email  = local.issuer_email
        server = "https://acme-v02.api.letsencrypt.org/directory"
        privateKeySecretRef = {
          name = "letsencrypt-production-private-key"
        }
        solvers = [{
          dns01 = {
            cloudflare = {
              apiTokenSecretRef = {
                name = kubernetes_secret.cloudflare_api_token.metadata.0.name
                key  = local.cloudflare_secret_key_name
              }
            }
          }
        }]
      }
    }
  }
  depends_on = [kubernetes_manifest.crds]
}

resource "kubernetes_manifest" "staging_issuer" {
  manifest = {
    apiVersion = "cert-manager.io/v1"
    kind       = "ClusterIssuer"
    metadata = {
      name = "letsencrypt-staging"
    }
    spec = {
      acme = {
        email  = local.issuer_email
        server = "https://acme-staging-v02.api.letsencrypt.org/directory"
        privateKeySecretRef = {
          name = "letsencrypt-staging-private-key"
        }
        solvers = [{
          dns01 = {
            cloudflare = {
              apiTokenSecretRef = {
                name = kubernetes_secret.cloudflare_api_token.metadata.0.name
                key  = local.cloudflare_secret_key_name
              }
            }
          }
        }]
      }
    }
  }
  depends_on = [kubernetes_manifest.crds]
}
