# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

resource "kubernetes_namespace" "cert_manager" {
  metadata {
    name = "cert-manager"
    labels = {
      # https://kubernetes.io/docs/concepts/security/pod-security-standards/
      "pod-security.kubernetes.io/enforce" = "restricted"
    }
  }
}

# https://artifacthub.io/packages/helm/cert-manager/cert-manager
# https://cert-manager.io/docs/installation/helm/
# https://helm.sh/docs/chart_best_practices/custom_resource_definitions/
# https://github.com/cert-manager/cert-manager/blob/master/deploy/charts/cert-manager/values.yaml
resource "helm_release" "cert_manager" {
  name       = "cert-manager"
  namespace  = kubernetes_namespace.cert_manager.metadata.0.name
  repository = "https://charts.jetstack.io"
  chart      = "cert-manager"
  version    = "1.14.4"

  skip_crds = true

  wait          = true
  wait_for_jobs = true

  values = [yamlencode({
    # CRDs are managed by Terraform
    crds = {
      enabled = false
    }

    global = {
      leaderElection = {
        # kube-system is managed by Autopilot so we move the leader election
        # namespace to cert-manager
        namespace = kubernetes_namespace.cert_manager.metadata.0.name
      }
    }
  })]

  depends_on = [kubernetes_manifest.crds]
}

resource "kubernetes_manifest" "crds" {
  for_each = local.crd_files
  manifest = yamldecode(file("${local.crd_path}${each.key}"))
}
